package com.example.paul.databaseexample;

/**
 * Created by Paul Cardenas on 5/4/2017.
 */

public class ItemConfig {

    //JSON URL
    public static final String DATA_URL = "http://www.controlapp.co.nf/Db/GetItems.php";

    //Tags used in the JSON String
    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_FACTORY = "factory";
    public static final String TAG_AMOUNT = "amount";
    public static final String TAG_DESCRIPTION = "description";

    //JSON array name
    public static final String JSON_ARRAY = "result";
}
