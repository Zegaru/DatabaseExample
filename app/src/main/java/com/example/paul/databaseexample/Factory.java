package com.example.paul.databaseexample;

/**
 * Created by Paul on 5/1/2017.
 */

public class Factory {

    public String name;
    public String phone;
    public String amount;
    public String id;

    public Factory(String name, String phone, String amount, String id) {
        this.name = name;
        this.phone = phone;
        this.amount = amount;
        this.id = id;
    }
}
