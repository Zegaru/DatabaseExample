package com.example.paul.databaseexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Paul Cardenas on 5/1/2017.
 */

public class RequestsAdapter extends ArrayAdapter<Request>{

    public RequestsAdapter(Context context, ArrayList<Request> request) {
        super(context, 0, request);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Request request = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_request, parent, false);
        }

        TextView tv_id = (TextView) convertView.findViewById(R.id.request_id);
        TextView tv_Address = (TextView) convertView.findViewById(R.id.requesT_address);
        TextView tv_Date = (TextView) convertView.findViewById(R.id.request_date);
        RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.request_layout);

        tv_id.setText(request.id);
        tv_Address.setText(request.address);
        tv_Date.setText(request.date);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(getContext(), "En construcción...", Toast.LENGTH_SHORT).show();
            }
        });

        return convertView;
    }
}
