package com.example.paul.databaseexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class ClientActivity extends AppCompatActivity {

    //To get clients from server
    private JSONArray result;

    //Variables to get requests data
    public static String head_id;
    public static String head_client;
    public static String head_address;
    public static String head_date;

    Button request_button;
    ListView requestList;
    RequestsAdapter adapter_request;
    ArrayList<Request> requestArrayList = new ArrayList<Request>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_client);

        //Creates dummy list for items.
        /*
        for (int i = 1; i<5; i++) {
            requestArrayList.add(new Request(""+(i*2-1), "Another address", "15-08-2017", i+""));
        }

        adapter_request = new RequestsAdapter(ClientActivity.this, requestArrayList);
        requestList = (ListView) findViewById(R.id.client_list_requests);
        requestList.setAdapter(adapter_request);*/

        getRequests();

        request_button = (Button) findViewById(R.id.client_button_request);
        request_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(ClientActivity.this, RequestActivity.class);
                startActivity(intent);
            }
        });
    }

    //Retrieve data stuff. Add items to the list.
    public void getRequests(){

        StringRequest stringRequest = new StringRequest(RequestConfig.DATA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response);

                        JSONObject j = null;
                        try {
                            requestArrayList.clear();
                            j = new JSONObject(response);
                            result = j.getJSONArray(RequestConfig.JSON_ARRAY);
                            setRequests(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", "Network");
                        Toast.makeText(ClientActivity.this, "Error de red 001: "+error, Toast.LENGTH_SHORT).show();
                    }
                });
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setRequests(JSONArray j) {

        for (int i=0; i<j.length(); i++) {

            try {

                JSONObject json = j.getJSONObject(i);

                head_id = json.getString(RequestConfig.TAG_ID);
                head_client = json.getString(RequestConfig.TAG_CLIENT);
                head_address = json.getString(RequestConfig.TAG_ADDRESS);
                head_date = json.getString(RequestConfig.TAG_DATE);

                Request request = new Request(head_client, head_address, head_date, head_id);
                requestArrayList.add(request);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(ClientActivity.this, "Error de red 002: "+e, Toast.LENGTH_SHORT).show();
            }
        }
        adapter_request = new RequestsAdapter(ClientActivity.this, requestArrayList);
        requestList = (ListView) findViewById(R.id.client_list_requests);
        requestList.setAdapter(adapter_request);
    }
}
