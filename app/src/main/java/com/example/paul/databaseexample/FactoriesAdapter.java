package com.example.paul.databaseexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Paul on 5/1/2017.
 */

public class FactoriesAdapter extends ArrayAdapter<Factory> {

    public FactoriesAdapter(Context context, ArrayList<Factory> factory) {
        super(context, 0, factory);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Factory factory = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_factory, parent, false);
        }

        TextView tv_id = (TextView) convertView.findViewById(R.id.factory_id);
        TextView tv_Name = (TextView) convertView.findViewById(R.id.factory_name);
        TextView tv_Phone = (TextView) convertView.findViewById(R.id.factory_phone);
        TextView tv_Amount = (TextView) convertView.findViewById(R.id.factory_amount);

        tv_id.setText(factory.id);
        tv_Name.setText(factory.name);
        tv_Phone.setText(factory.phone);
        tv_Amount.setText(factory.amount);

        /*
        RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.factory_layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "En construcción", Toast.LENGTH_SHORT).show();
            }
        });
        */

        return convertView;
    }
}
