package com.example.paul.databaseexample;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Paul on 5/1/2017.
 */

public class ItemAdapter extends ArrayAdapter<Item>{

    public ItemAdapter(Context context, ArrayList<Item> item) {
        super(context, 0, item);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Item item = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_item, parent, false);
        }

        TextView tv_id = (TextView) convertView.findViewById(R.id.item_id);
        TextView tv_Name = (TextView) convertView.findViewById(R.id.item_name);
        TextView tv_Factory = (TextView) convertView.findViewById(R.id.item_factory);
        TextView tv_Amount = (TextView) convertView.findViewById(R.id.item_amount);
        TextView tv_Description = (TextView) convertView.findViewById(R.id.item_description);

        tv_id.setText(item.id);
        tv_Name.setText(item.name);
        tv_Factory.setText(item.factory);
        tv_Amount.setText(item.amount);
        tv_Description.setText(item.description);

        /*
        RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.item_layout);
        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(getContext(), "En construcción", Toast.LENGTH_SHORT).show();
            }
        });
        */

        return convertView;
    }
}
