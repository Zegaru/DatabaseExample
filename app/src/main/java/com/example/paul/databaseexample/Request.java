package com.example.paul.databaseexample;

/**
 * Created by Paul on 5/1/2017.
 */

public class Request {

    public String client_id;
    public String address;
    public String date;
    public String id;

    public Request(String client_id, String address, String date, String id) {
        this.client_id = client_id;
        this.address = address;
        this.date = date;
        this.id = id;
    }
}
