package com.example.paul.databaseexample;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.MultiAutoCompleteTextView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.AuthFailureError;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import static com.android.volley.Request.Method.POST;

public class RequestActivity extends AppCompatActivity implements AdapterView.OnItemSelectedListener{

    //To get data from server.
    private JSONArray result;

    //Variables to get items.
    public static String item_id;
    public static String item_name;
    public static String item_factory;
    public static String item_amount;
    public static String item_description;

    //Variables for magic to happen.
    AutoCompleteTextView item_1;
    AutoCompleteTextView item_2;
    AutoCompleteTextView item_3;

    private ArrayList<String> itemList = new ArrayList<String>();
    EditText amount_1;
    EditText amount_2;
    EditText amount_3;
    String head_id = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_request);

        //Code for spinner to work
        List<String> spinnerArray =  new ArrayList<String>();
        spinnerArray.add("1");
        spinnerArray.add("2");
        spinnerArray.add("3");

        final ArrayAdapter<String> adapter = new ArrayAdapter<String>(
                this, R.layout.support_simple_spinner_dropdown_item, spinnerArray);

        Spinner spinner = (Spinner) findViewById(R.id.request_spinner);
        spinner.setOnItemSelectedListener(this);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);

        getItems();
        //sendHead();
    }

    public void setVisibility(int status) {

        if (status == 0) {

            item_2.setVisibility(View.INVISIBLE);
            item_3.setVisibility(View.INVISIBLE);
        } else if (status == 1) {

            item_2.setVisibility(View.VISIBLE);
            item_3.setVisibility(View.INVISIBLE);
        } else if (status == 2) {

            item_2.setVisibility(View.VISIBLE);
            item_3.setVisibility(View.VISIBLE);
        }
    }

    //Spinner stuff
    public void onItemSelected(AdapterView<?> parent, View view,
                               int pos, long id) {
        // parent.getItemAtPosition(pos)

        item_2 = (AutoCompleteTextView) findViewById(R.id.request_edit_item_2);
        item_3 = (AutoCompleteTextView) findViewById(R.id.request_edit_item_3);
        amount_1 = (EditText) findViewById(R.id.request_edit_amount);
        amount_2 = (EditText) findViewById(R.id.request_edit_amount_2);
        amount_3 = (EditText) findViewById(R.id.request_edit_amount_3);

        switch (pos) {
            case 0:
                setVisibility(pos);
                amount_2.setVisibility(View.INVISIBLE);
                amount_3.setVisibility(View.INVISIBLE);
                break;
            case 1:
                setVisibility(pos);
                amount_2.setVisibility(View.VISIBLE);
                amount_3.setVisibility(View.INVISIBLE);
                break;
            case 2:
                setVisibility(pos);
                amount_2.setVisibility(View.VISIBLE);
                amount_3.setVisibility(View.VISIBLE);
                break;
        }
    }

    public void onNothingSelected(AdapterView<?> parent) {
        // Another interface callback
    }

    //Retrieve data stuff. Add items to the list.
    public void getItems(){

        StringRequest stringRequest = new StringRequest(ItemConfig.DATA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response);

                        JSONObject j = null;
                        try {
                            itemList.clear();
                            j = new JSONObject(response);
                            result = j.getJSONArray(ItemConfig.JSON_ARRAY);
                            setItems(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", "Network");
                        Toast.makeText(RequestActivity.this, "Error de red 001: "+error, Toast.LENGTH_SHORT).show();
                    }
                });
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setItems(JSONArray j) {

        for (int i=0; i<j.length(); i++) {

            try {

                JSONObject json = j.getJSONObject(i);

                item_id = json.getString(ItemConfig.TAG_ID);
                item_name = json.getString(ItemConfig.TAG_NAME);
                item_factory = json.getString(ItemConfig.TAG_FACTORY);
                item_amount = json.getString(ItemConfig.TAG_AMOUNT);
                item_description = json.getString(ItemConfig.TAG_DESCRIPTION);

                itemList.add(item_name);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(RequestActivity.this, "Error de red 002: "+e, Toast.LENGTH_SHORT).show();
            }
        }
        item_1 = (AutoCompleteTextView) findViewById(R.id.request_edit_item);
        item_2 = (AutoCompleteTextView) findViewById(R.id.request_edit_item_2);
        item_3 = (AutoCompleteTextView) findViewById(R.id.request_edit_item_3);

        ArrayAdapter adapter_1 = new ArrayAdapter<String>(RequestActivity.this, android.R.layout.simple_list_item_1, itemList);
        item_1.setAdapter(adapter_1);
        ArrayAdapter adapter_2 = new ArrayAdapter<String>(RequestActivity.this, android.R.layout.simple_list_item_1, itemList);
        item_2.setAdapter(adapter_2);
        ArrayAdapter adapter_3 = new ArrayAdapter<String>(RequestActivity.this, android.R.layout.simple_list_item_1, itemList);
        item_3.setAdapter(adapter_3);
    }

    private void sendHead(){

        Intent intent = getIntent();
        final String client_id = intent.getStringExtra("client_id");
        final String address = intent.getStringExtra("address");
        final Button request_button = (Button) findViewById(R.id.request_button);

        if (!client_id.equals("")) {
            request_button.setOnClickListener(new View.OnClickListener(){
                @Override
                public void onClick(View view) {

                        request_button.setClickable(false);

                        RequestQueue queue = Volley.newRequestQueue(RequestActivity.this);
                        String url = "http://www.controlapp/Db/SendHead.php";

                        final StringRequest stringRequest = new StringRequest(POST, url,
                                new Response.Listener<String>() {
                                    @Override
                                    public void onResponse(String response) {
                                        Log.d("idResponse", response);

                                        if (!response.isEmpty() & !response.equals("0") & !response.equals("null")) {
                                            head_id = response;
                                            Spinner spinner = (Spinner) findViewById(R.id.request_spinner);
                                            String new_cant = spinner.toString();
                                            sendBody(new_cant);
                                        } else if (response.equals("empty")) {
                                            Toast.makeText(RequestActivity.this, "Completa los datos", Toast.LENGTH_SHORT).show();
                                        } else {
                                            Toast.makeText(RequestActivity.this, response, Toast.LENGTH_SHORT).show();
                                            request_button.setClickable(true);
                                        }
                                    }
                                }, new Response.ErrorListener() {
                            @Override
                            public void onErrorResponse(VolleyError error) {
                                Log.d("Error", error + "");
                                Toast.makeText(RequestActivity.this, "Error de red",
                                        Toast.LENGTH_SHORT).show();
                                request_button.setClickable(true);
                            }
                        }) {
                            @Override
                            protected Map<String, String> getParams() throws AuthFailureError {

                                Map<String, String> params = new HashMap<>();
                                params.put("client_id", client_id);
                                params.put("address", address);
                                return params;
                            }
                        };
                        stringRequest.setShouldCache(false);
                        queue.add(stringRequest);
                    }
                }

            );}
    }

    private void sendBody(final String cant){
            RequestQueue queue = Volley.newRequestQueue(RequestActivity.this);
            String url = "http://www.controlapp/Db/SendBody.php";

            final StringRequest stringRequest = new StringRequest(POST, url,
                    new Response.Listener<String>() {
                        @Override
                        public void onResponse(String response) {
                            Log.d("idResponse", response);

                            if (!response.isEmpty() & !response.equals("0") & !response.equals("null")) {

                                int more = Integer.parseInt(cant);
                                if (more>1) {
                                    sendBody(Integer.toString(more-1));
                                } else {
                                    Toast.makeText(RequestActivity.this, "Pedido enviado",
                                            Toast.LENGTH_SHORT).show();

                                    Intent intent = new Intent(RequestActivity.this, ClientActivity.class);
                                    intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                    startActivity(intent);
                                }
                            } else {
                                Toast.makeText(RequestActivity.this, response, Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    Log.d("Error", error + "");
                    Toast.makeText(RequestActivity.this, "Error de red",
                            Toast.LENGTH_SHORT).show();
                }
            }) {
                @Override
                protected Map<String, String> getParams() throws AuthFailureError {

                    item_1 = (AutoCompleteTextView) findViewById(R.id.request_edit_item);
                    amount_1 = (EditText) findViewById(R.id.request_edit_amount);

                    Map<String, String> params = new HashMap<>();
                    params.put("head_id", head_id);
                    params.put("item_name", item_1.getText().toString());
                    params.put("amount", amount_1.getText().toString());
                    return params;
                }
            };
            stringRequest.setShouldCache(false);
            queue.add(stringRequest);
    }
}
