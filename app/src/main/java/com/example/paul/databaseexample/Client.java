package com.example.paul.databaseexample;

/**
 * Created by Paul on 4/30/2017.
 */

public class Client {

    public String name;
    public String address;
    public String balance;
    public String credit;
    public String discount;
    public String id;

    public Client(String name, String address, String balance, String credit, String discount, String id) {
        this.name = name;
        this.address = address;
        this.balance = balance;
        this.credit = credit;
        this.discount = discount;
        this.id = id;
    }
}
