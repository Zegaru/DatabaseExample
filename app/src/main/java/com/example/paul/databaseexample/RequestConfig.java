package com.example.paul.databaseexample;

/**
 * Created by Paul Cardenas on 5/5/2017.
 */

public class RequestConfig {

    //JSON URL
    public static final String DATA_URL = "http://www.controlapp.co.nf/Db/GetHeads.php";

    //Tags used in the JSON String
    public static final String TAG_ID = "id";
    public static final String TAG_CLIENT = "client_id";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_DATE = "date";

    //JSON array name
    public static final String JSON_ARRAY = "result";
}
