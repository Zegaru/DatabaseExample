package com.example.paul.databaseexample;

/**
 * Created by Paul on 5/1/2017.
 */

public class Item {

    public String name;
    public String factory;
    public String amount;
    public String description;
    public String id;

    public Item(String name, String factory, String amount, String description, String id) {
        this.name = name;
        this.factory = factory;
        this.amount = amount;
        this.description = description;
        this.id = id;
    }
}
