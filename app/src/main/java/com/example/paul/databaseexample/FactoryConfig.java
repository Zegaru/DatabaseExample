package com.example.paul.databaseexample;

/**
 * Created by Paul Cardenas on 5/4/2017.
 */

public class FactoryConfig {

    //JSON URL
    public static final String DATA_URL = "http://www.controlapp.co.nf/Db/GetFactories.php";

    //Tags used in the JSON String
    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_PHONE = "phone";
    public static final String TAG_AMOUNT = "amount";

    //JSON array name
    public static final String JSON_ARRAY = "result";
}
