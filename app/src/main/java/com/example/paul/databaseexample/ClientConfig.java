package com.example.paul.databaseexample;

/**
 * Created by Paul Cardenas on 5/3/2017.
 */

public class ClientConfig {

    //JSON URL
    public static final String DATA_URL = "http://www.controlapp.co.nf/Db/GetClients.php";

    //Tags used in the JSON String
    public static final String TAG_ID = "id";
    public static final String TAG_NAME = "name";
    public static final String TAG_ADDRESS = "address";
    public static final String TAG_BALANCE = "balance";
    public static final String TAG_CREDIT = "credit";
    public static final String TAG_DISCOUNT = "discount";
    public static final String TAG_TIME = "date";

    //JSON array name
    public static final String JSON_ARRAY = "result";
}
