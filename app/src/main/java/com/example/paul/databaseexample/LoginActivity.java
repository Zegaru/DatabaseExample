package com.example.paul.databaseexample;

import android.content.Intent;
import android.database.DataSetObserver;
import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Adapter;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;

public class LoginActivity extends AppCompatActivity {

    //To get clients from server
    private JSONArray result;

    //Variables to get clients data
    public static String client_id;
    public static String client_name;
    public static String client_address;
    public static String client_balance;
    public static String client_credit;
    public static String client_discount;
    public static String client_time;

    Button button_client;
    Button button_admin;
    ListView list_clients;
    ClientsAdapter adapter_clients;
    ArrayList<Client> arrayList = new ArrayList<Client>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        /*
        Log.d("Why", "At least it started");
        //Creates dummy list for clients.
        for (int i = 1; i<5; i++) {
            Log.d("Why", "It created item "+i);
            arrayList.add(new Client("Cliente ", "Calle "+i+" Lote 2"+i, "S/."+(i*27120), "S/."+i+"000", i+"0%", ""+i));
        }

        //Links adapter with list.
        Log.d("Why", "Adapter stuff begun");
        adapter_clients = new ClientsAdapter(LoginActivity.this, arrayList);
        list_clients.setAdapter(adapter_clients);
        Log.d("Why", "Adapter stuff ended");*/

        list_clients = (ListView) findViewById(R.id.login_list);

        //Fills list from database.
        getClients();

        button_client = (Button) findViewById(R.id.login_button_client);
        button_client.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Shows list of clients.
                if (list_clients.getVisibility() == View.VISIBLE) {
                    list_clients.setVisibility(View.INVISIBLE);
                } else if (list_clients.getVisibility() == View.INVISIBLE) {
                    list_clients.setVisibility(View.VISIBLE);
                }
            }
        });

        button_admin = (Button) findViewById(R.id.login_button_admin);
        button_admin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                //Toast.makeText(LoginActivity.this, "Lol", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(LoginActivity.this, AdminActivity.class);
                startActivity(intent);
            }
        });
    }

    //Retrieve data stuff. Add items to the list.
    public void getClients(){

        StringRequest stringRequest = new StringRequest(ClientConfig.DATA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response);

                        JSONObject j = null;
                        try {
                            arrayList.clear();
                            j = new JSONObject(response);
                            result = j.getJSONArray(ClientConfig.JSON_ARRAY);
                            setClients(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", "Network");
                        Toast.makeText(LoginActivity.this, "Error de red 001: "+error, Toast.LENGTH_SHORT).show();
                    }
                });
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setClients(JSONArray j) {

        for (int i=0; i<j.length(); i++) {

            try {

                JSONObject json = j.getJSONObject(i);

                client_id = json.getString(ClientConfig.TAG_ID);
                client_name = json.getString(ClientConfig.TAG_NAME);
                client_address = json.getString(ClientConfig.TAG_ADDRESS);
                client_balance = json.getString(ClientConfig.TAG_BALANCE);
                client_credit = json.getString(ClientConfig.TAG_CREDIT);
                client_discount = json.getString(ClientConfig.TAG_DISCOUNT);
                client_time = json.getString(ClientConfig.TAG_TIME);

                Client client = new Client(client_name, client_address, client_balance, client_credit, client_discount, client_id);
                arrayList.add(client);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(LoginActivity.this, "Error de red 002: "+e, Toast.LENGTH_SHORT).show();
            }
        }
        adapter_clients = new ClientsAdapter(LoginActivity.this, arrayList);
        list_clients = (ListView) findViewById(R.id.login_list);
        list_clients.setAdapter(adapter_clients);
    }
}
