package com.example.paul.databaseexample;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ListView;
import android.widget.TabHost;
import android.widget.Toast;

import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class AdminActivity extends AppCompatActivity {

    //To get data from server.
    private JSONArray result;
    private JSONArray result_x;

    //Variables to get factories.
    public static String factory_id;
    public static String factory_name;
    public static String factory_phone;
    public static String factory_amount;

    //Variables to get items.
    public static String item_id;
    public static String item_name;
    public static String item_factory;
    public static String item_amount;
    public static String item_description;

    ListView list_item;
    ListView list_factory;
    ItemAdapter adapter_items;
    ArrayList<Item> itemList = new ArrayList<Item>();
    ArrayList<Factory> factoryArrayList = new ArrayList<Factory>();
    FactoriesAdapter adapter_factories;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_admin);

        //Setting up TabHost
        final TabHost tabHost = (TabHost) findViewById(R.id.admin_tabhost);
        tabHost.setup();

        TabHost.TabSpec spec = tabHost.newTabSpec("Tab One");
        spec.setContent(R.id.tab_items);
        spec.setIndicator("Artículos");
        tabHost.addTab(spec);

        spec = tabHost.newTabSpec("Tab Two");
        spec.setContent(R.id.tab_factories);
        spec.setIndicator("Fábricas");
        tabHost.addTab(spec);

        //Creates dummy list for items.
        /*
        for (int i = 1; i<5; i++) {
            itemList.add(new Item("Artículo "+i, ""+i, (i*13)+"000", "Descripción del artículo "+i, i+""));
        }*/

        getFactories();
        getItems();

        //Creates dummy list for factories.
        /*
        for (int i = 1; i<5; i++) {
            factoryArrayList.add(new Factory("Fábrica "+i, "95058973", ""+(i*7), i+""));
        }*/

        //Links adapters with lists.
        /*
        adapter_clients = new ItemAdapter(AdminActivity.this, itemList);
        list_item = (ListView) findViewById(R.id.admin_list_item);
        list_item.setAdapter(adapter_clients);*/

        /*
        adapter_factories = new FactoriesAdapter(AdminActivity.this, factoryArrayList);
        list_factory = (ListView) findViewById(R.id.admin_list_factory);
        list_factory.setAdapter(adapter_factories);*/
    }

    //Retrieve data stuff. Add factories to the list.
    public void getFactories(){

        StringRequest stringRequest = new StringRequest(FactoryConfig.DATA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response);

                        JSONObject j = null;
                        try {
                            factoryArrayList.clear();
                            j = new JSONObject(response);
                            result = j.getJSONArray(FactoryConfig.JSON_ARRAY);
                            setFactories(result);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", "Network");
                        Toast.makeText(AdminActivity.this, "Error de red 001: "+error, Toast.LENGTH_SHORT).show();
                    }
                });
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setFactories(JSONArray j) {

        for (int i=0; i<j.length(); i++) {

            try {

                JSONObject json = j.getJSONObject(i);

                factory_id = json.getString(FactoryConfig.TAG_ID);
                factory_name = json.getString(FactoryConfig.TAG_NAME);
                factory_phone = json.getString(FactoryConfig.TAG_PHONE);
                factory_amount = json.getString(FactoryConfig.TAG_AMOUNT);

                Factory factory = new Factory(factory_name, factory_phone,factory_amount, factory_id);
                factoryArrayList.add(factory);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(AdminActivity.this, "Error de red 002: "+e, Toast.LENGTH_SHORT).show();
            }
        }
        adapter_factories = new FactoriesAdapter(AdminActivity.this, factoryArrayList);
        list_factory = (ListView) findViewById(R.id.admin_list_factory);
        list_factory.setAdapter(adapter_factories);
    }

    //Retrieve data stuff. Add items to the list.
    public void getItems(){

        StringRequest stringRequest = new StringRequest(ItemConfig.DATA_URL,
                new Response.Listener<String>() {
                    @Override
                    public void onResponse(String response) {
                        Log.d("response", response);

                        JSONObject j = null;
                        try {
                            itemList.clear();
                            j = new JSONObject(response);
                            result_x = j.getJSONArray(ItemConfig.JSON_ARRAY);
                            setItems(result_x);
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                    }
                },
                new Response.ErrorListener() {
                    @Override
                    public void onErrorResponse(VolleyError error) {
                        Log.d("Error", "Network");
                        Toast.makeText(AdminActivity.this, "Error de red 001: "+error, Toast.LENGTH_SHORT).show();
                    }
                });
        stringRequest.setShouldCache(false);
        RequestQueue requestQueue = Volley.newRequestQueue(this);
        requestQueue.add(stringRequest);
    }

    private void setItems(JSONArray j) {

        for (int i=0; i<j.length(); i++) {

            try {

                JSONObject json = j.getJSONObject(i);

                item_id = json.getString(ItemConfig.TAG_ID);
                item_name = json.getString(ItemConfig.TAG_NAME);
                item_factory = json.getString(ItemConfig.TAG_FACTORY);
                item_amount = json.getString(ItemConfig.TAG_AMOUNT);
                item_description = json.getString(ItemConfig.TAG_DESCRIPTION);

                Item item = new Item(item_name, item_factory, item_amount, item_description, item_id);
                itemList.add(item);
            } catch (JSONException e) {
                e.printStackTrace();
                Toast.makeText(AdminActivity.this, "Error de red 002: "+e, Toast.LENGTH_SHORT).show();
            }
        }
        adapter_items = new ItemAdapter(AdminActivity.this, itemList);
        list_item = (ListView) findViewById(R.id.admin_list_item);
        list_item.setAdapter(adapter_items);
    }
}
