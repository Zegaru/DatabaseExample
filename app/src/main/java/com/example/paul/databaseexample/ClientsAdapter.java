package com.example.paul.databaseexample;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

/**
 * Created by Paul Cardenas on 4/30/2017.
 */

public class ClientsAdapter extends ArrayAdapter<Client>{

    public ClientsAdapter(Context context, ArrayList<Client> client) {
        super(context, 0, client);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        final Client client = getItem(position);

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_client, parent, false);
        }

        TextView tv_Name = (TextView) convertView.findViewById(R.id.client_name);
        TextView tv_Address = (TextView) convertView.findViewById(R.id.client_address);
        TextView tv_Balance = (TextView) convertView.findViewById(R.id.client_balance);
        RelativeLayout layout = (RelativeLayout) convertView.findViewById(R.id.client_layout);

        tv_Name.setText(client.name);
        tv_Address.setText(client.address);
        tv_Balance.setText(client.balance);

        layout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(getContext(), ClientActivity.class);
                intent.putExtra("client_id", client.id);
                intent.putExtra("address", client.address);
                getContext().startActivity(intent);
            }
        });

        return convertView;
    }
}
